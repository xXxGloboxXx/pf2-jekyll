---
title: Druide
titleEN: Druid
source: "Playtest Pathfinder"
toc: true
layout: classe

#PARTIE PROPRE AUX CLASSES

keyAbility: Sag
hp: 8

#Degrés: NE Ent Exp Maî Lég
profPerception: Ent
profFortitude: Exp
profReflex: Ent
profWill: Exp

skills: 3
weapons: "Ent pour toutes les armes simples et le cimeterre"
armor: "Ent pour les armures légères et intermédiaires non métalliques"
spells: "Ent pour les jets, les DD et les jets d'attaque des sorts primaires"
signatureSkills:
  - Artisanat
  - Nature
  - Survie
  - une compétence déterminée par l'ordre
---

NOTE : le skills = 3 vient d'un errata ; il est correct !

Blah blah blah... description du druide


ERRATA !!!
Page 79—For the druid, in the Proficiencies section of the
sidebar, under Skills, change “4” to “3”.





{:.bloc .action2}
> ### Forme élémentaire **Don 10**
> *Druide, Ordre sauvage*  
> **Condition** Forme sauvage
> 
> ---
> Vous pouvez lancer forme élémentaire en dépensant un point de votre réserve
> de forme sauvage.  
> Test avec plusieurs paragraphes.


{:.bloc .action1}
> ### Forme élémentaire **Don 10**
> *Druide, Ordre sauvage*  
> **Condition** Forme sauvage
> 
> ---
> Vous pouvez lancer forme élémentaire en dépensant un point de votre réserve
> de forme sauvage.  
> Test avec plusieurs paragraphes.  
> Ça a l'air de fonctionner. On peut aussi ajouter des résultats de jds.  
> **Échec critique** : t'es mort.  
> **Échec** : t'es à moitié mort.  
> **Réussite** : tu saignes juste un peu.  
> **Réussite critique** : tu n'as rien. 

