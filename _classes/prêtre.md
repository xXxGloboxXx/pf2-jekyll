---
title: Prêtre
titleEN: Cleric
source: "Playtest Pathfinder"
toc: true
layout: classe

#PARTIE PROPRE AUX CLASSES

keyAbility: Int
hp: 8

#Degrés: NE Ent Exp Maî Lég
profPerception: Ent
profFortitude: Exp
profReflex: Exp
profWill: Ent

skills: 2
weapons: "Ent pour toutes les armes simples, Ent pour les bombes alchimiques"
armor: "Ent pour les armures légères"
spells:
signatureSkills:
  - Arcanes
  - Artisanat
  - Médecine
---

Blah blah blah... description


!! ERRATA

Page 73—In Table 3–10: Domains, in the Might domain,
change “Enduring strength” to “Enduring might”.