---
title: Magicien
titleEN: Wizard
source: "Playtest Pathfinder"
toc: true
layout: classe

#PARTIE PROPRE AUX CLASSES

keyAbility: Int
hp: 8

#Degrés: NE Ent Exp Maî Lég
profPerception: Ent
profFortitude: Exp
profReflex: Exp
profWill: Ent

skills: 2
weapons: "Ent pour toutes les armes simples, Ent pour les bombes alchimiques"
armor: "Ent pour les armures légères"
spells:
signatureSkills:
  - Arcanes
  - Artisanat
  - Médecine

index:
  - key: NOM_CAPACITE (capacité du XX_CLASSE)
    anchor: "LIEN_CAPACITE"

---

* Liste pour toc
{:toc .float-right .ml-2}

*Blablabla Intro*

## Jouer un XX_CLASSE

Les joueurs qui incarnent un personnage XX_CLASSE pourraient adopter les approches suivantes :

* Au combat, Blabla au présent
* Pendant les rencontres sociales,
* Dans les phases d’explorations,
* Pendant votre temps libre,

## Le rôle du XX_CLASSE
Blablabla

**Si vous êtes un XX_CLASSE, alors, très probablement...**
* Blabla au présent

**Quant aux autres...**
* Blabla au présent

## Capacités de classe
Voici quelques termes spécifiques utilisés dans les descriptions des capacités et des dons du XX_CLASSE.

**MOT_CLEF** BLABLA

Vous gagnez les capacités suivantes en tant que XX_CLASSE. 

{: .tabletitle}
Progression du XX_CLASSE

{: .table .table-striped .table-sm .table-hover}
| Niveau | Capacités de classe |
|:-------|:--------------------|
| 1 | Background, [don de XX_CLASSE](#dons-de-XX_CLASSE), [don d'ascendance](#dons-dascendance), formations initiales |
| 2 | [Don de XX_CLASSE](#dons-de-XX_CLASSE), [don de compétence](#dons-de-compétence) |
| 3 | [Augmentation de compétence](#augmentation-de-compétence), [don général](#dons-généraux) |
| 4 | [Don de XX_CLASSE](#dons-de-XX_CLASSE), [don de compétence](#dons-de-compétence) |
| 5 | [Accroissement de caractéristique](#accroissement-de-caractéristique), [augmentation de compétence](#augmentation-de-compétence), [don d'ascendance](#dons-dascendance) |
| 6 | [Don de XX_CLASSE](#dons-de-XX_CLASSE), [don de compétence](#dons-de-compétence) |
| 7 | [Augmentation de compétence](#augmentation-de-compétence), [don général](#dons-généraux) |
| 8 | [Don de XX_CLASSE](#dons-de-XX_CLASSE), [don de compétence](#dons-de-compétence) |
| 9 | [Augmentation de compétence](#augmentation-de-compétence), [don d'ascendance](#dons-dascendance) |
| 10 | [Accroissement de caractéristique](#accroissement-de-caractéristique), [don de XX_CLASSE](#dons-de-XX_CLASSE), [don de compétence](#dons-de-compétence) |
| 11 | [Augmentation de compétence](#augmentation-de-compétence), [don général](#dons-généraux) |
| 12 | [Don de XX_CLASSE](#dons-de-XX_CLASSE), [don de compétence](#dons-de-compétence) |
| 13 | [Augmentation de compétence](#augmentation-de-compétence), [don d'ascendance](#dons-dascendance) |
| 14 | [Don de XX_CLASSE](#dons-de-XX_CLASSE), [don de compétence](#dons-de-compétence) |
| 15 | [Accroissement de caractéristique](#accroissement-de-caractéristique), [augmentation de compétence](#augmentation-de-compétence), [don général](#dons-généraux) |
| 16 | [Don de XX_CLASSE](#dons-de-XX_CLASSE), [don de compétence](#dons-de-compétence) |
| 17 | [Augmentation de compétence](#augmentation-de-compétence), [don d'ascendance](#dons-dascendance) |
| 18 | [Don de XX_CLASSE](#dons-de-XX_CLASSE), [don de compétence](#dons-de-compétence) |
| 19 | [Augmentation de compétence](#augmentation-de-compétence), [don général](#dons-généraux) |
| 20 | [Accroissement de caractéristique](#accroissement-de-caractéristique), [don de XX_CLASSE](#dons-de-XX_CLASSE), [don de compétence](#dons-de-compétence) |

### XXCAPACITE_DE_CLASSE
Blablabla

{:.block .action1}
> ### Alchimie rapide
> 
> {: .traits }
> Alchimiste
> 
> **Coût** 1 Point de résonance
> 
> **Condition** Vous devez posséder des outils d'alchimiste (voir <mark>page 184</mark>), la formule de l'objet alchimique que vous créez et une main libre.
> 
> ---
>
> Vous créez un objet alchimique commun qui est de votre niveau ou d'un niveau inférieur sans devoir dépenser le coût monétaire habituel pour des réactifs alchimiques et sans avoir à effectuer un test d'Artisanat. Cet objet possède le trait infusion mais il ne reste efficace que jusqu'au début de votre prochain tour. Passé ce délai, l'objet devient interte et n'a plus d'effet. Si vous dépensez de la résonance pour cette capacité alors que votre réserve est vide et ratez le test (voir page <mark>  292</mark>), vous ne pouvez pas plus utiliser l'action Alchimie rapide à nouveau avant vos prochaines préparations quotidiennes.

### Dons de compétence
Aux niveaux pairs, vous gagnez un don de compétence. Les dons de compétence se trouvent dans le Chapitre <mark>5</mark> et possèdent le trait compétence. Vous devez être au minimum entrainé dans la compétence correspondante pour pouvoir sélectionner un don de compétence.

### Dons généraux
Au niveau 3, et tous les 4 niveaux ensuite, vous gagnez un don général. Les dons généraux sont listés dans le Chapître <mark>5</mark>

### Augmentation de compétence
Au niveau 3, et tous les deux niveaux ensuite, vous obtenez une augmentation de compétence. Vous pouvez utiliser celle-ci :
* soit pour être entraîné dans une compétence dans laquelle vous n'étiez pas encore entraîné.
* soit pour devenir un expert dans une compétence dans laquelle vous êtes déjà entraîné.
* soit, si vous êtes au moins **niveau 7**, pour devenir un maître dans une compétence de signature, dans laquelle vous êtes déjà un expert.
* soit, si vous êtes au moins **niveau 15**, pour devenir légendaire dans une compétence de signature, dans laquelle vous êtes déjà un maître.

### Accroissement de caractéristique
Au niveau 5 et tous les 5 niveaux ensuite, vous pouvez accroitre 4 caractéristiques différentes. Vous pouvez utiliser ces accroissements pour augmenter une caractéristique au dessus de 18. Accroître une caractéristique l'augmente de 1 si elle se trouve déjà à 18 ou plus, ou de 2 si elle n'est pas encore à 18.

### Dons d'ascendance
En plus du don d'ascendance avec lequel vous commencez, vous gagnez un don d'ascendance au niveau 5 puis tous les 4 niveaux ensuite. La liste des dons d'ascendance disponibles pour vous se trouve au niveau de votre ascendance, dans le Chapitre <mark>2</mark>.

### Dons de XX_CLASSE
Au niveau 1 et à chaque niveau pair ensuite, vous obtenez un don de classe XX_CLASSE. Les dons de classe XX_CLASSE sont décrits à partir de la page <mark>48</mark>.