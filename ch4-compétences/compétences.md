---
title: "Règles générales"
source: "Playtest Pathfinder"
toc: true
---

Alors que vos valeurs de caractéristiques représentent votre talent brut et votre potentiel, les compétences démontrent votre entrainement et votre expérience. Chaque compétence recouvre plusieurs utilisations possibles et chacune des compétences est liée à une de vos caractéristiques.

Dans ce chapitre, vous allez en apprendre davantage sur les compétences, leur périmètre et les actions, les activités, les actions libres et les réactions qu’elles permettent.


## Les utilisations des compétences

Les actions, les activités, les actions libres et les réactions que vous pouvez accomplir avec chaque compétence donnée sont réparties entre celles que vous pouvez accomplir sans entrainement et celles qui nécessitent que vous soyez au moins entrainé pour les utiliser, comme indiqué dans la table ci-après. Les utilisations que vous pouvez en faire sans entrainement et lorsque vous êtes entrainé apparaissent dans des sections distinctes au sein de la description de chaque compétence.

Chacun peut utiliser la compétence sans entrainement, sauf circonstance ou condition particulière ou à moins que des effets ne l’empêchent de le faire. Vous ne pouvez utiliser les possibilités qu’offre la compétence en étant entraîné que si vous êtes au moins entraîné dans cette compétence et aucune circonstance, condition ou effet ne peut vous empêcher de l’utiliser de la sorte. Quelque fois, utiliser une compétence dans une situation spécifique peut vous obliger à disposer d’un degré d’entrainement plus poussé que ce qui figure dans le tableau. Par exemple, même si un barbare de haut niveau sans entrainement dans la compétence Arcanes pourrait sans doute l’utiliser pour se rappeler des connaissances relatives au souffle des différents dragons chromatiques, le MJ pourrait décider que se rappeler des connaissances à propos des théories derrière l’énergie magique du souffle des dragons est au-delà du périmètre des connaissances largement utilitaires et anecdotiques du barbare sur la manière de combattre les dragons. Le MJ décide si une tâche requiert un degré d’entrainement dans la compétence particulière, en partant d’entraîné jusqu’au degré légendaire.

Au fur et à mesure de votre progression de niveau, vous allez obtenir des dons généraux appelés dons de compétences, qui vous permettront souvent d’utiliser une compétence différemment. Les dons de compétence ont toujours le trait compétence. <mark>Ces dons apparaissent plus loin.</mark>

## Caractéristique liée

Chaque compétence est liée à une Caractéristique. C’est le modificateur de cette dernière qui doit être ajoutée lorsque vous utilisez cette compétence. Frapper parmi les ombres d’une ville durant la nuit avec Discrétion utilise votre modificateur de Dextérité alors que naviguer parmi la myriade de personnalités et les intrigues politiques de la cour avec Société vous impose d’utiliser votre modificateur d’Intelligence, etc.

La Caractéristique liée à chaque compétence figure sur la table ci-après et dans une parenthèse suivant le nom de la compétence dans les descriptions qui suivent. Si le MJ pense que c’est approprié, il peut vous permettre d’utiliser le modificateur d’une autre Caractéristique pour un test de compétence ou lorsqu’il détermine votre DD dans cette compétence.

## Tests de compétence et DD opposé

Lorsque vous utilisez activement une compétence (généralement en vous engageant dans l’une de ses utilisations), vous allez souvent tenter un test de compétence en lançant un d20 et en ajoutant votre modificateur de compétence. Pour déterminer votre modificateur de compétence, ajoutez le modificateur de Caractéristique lié à cette compétence ainsi que tous les bonus et pénalités qui peuvent s’appliquer

Modificateur de compétence = modificateur de Caractéristique + modificateur de compétence + bonus de circonstance + bonus conditionnel + bonus d’objet + pénalité de circonstance + pénalité conditionnelle + pénalité d’objet + pénalités non typées

Quelquefois le DD du test de compétence figure dans la description de la compétence elle-même. D’autres fois, il faut que le MJ détermine le test de compétence en utilisant les lignes directrices du <mark>[chapitre 10 – Maîtriser une partie]</mark>.

Lorsque quelque chose ou quelqu’un teste votre compétence il doit réaliser un test contre le DD opposé de votre compétence. Il correspond à 10 + votre modificateur de compétence. Un DD de compétence opposé fonctionne comme n’importe quel DD lorsu’il s’agit de déterminer les effets de l’utilisation de la compétence par la créature opposée 

Comme pour les autres tests, les bonus peuvent augmenter le résultat de votre test et les pénalités peuvent le diminuer. <mark>[Voir page 290 au chapitre 9 : Jouer la partie pour davantage d’informations sur les modificateurs, les bonus et les pénalités.]</mark>

### Aider aux tests de compétence

De temps à autre, le MJ peut vous autoriser à utiliser une compétence pour aider un autre personnage à accomplir une tâche de manière plus efficace. Dans certaines situations, vous pouvez simplement accomplir une ou plus utilisations d’une compétence pour accorder au test de compétence d’une autre personne un bonus de circonstance. D’autres fois, aider le test de compétence d’un allié requiert une meilleure coordination et nécessite d’utiliser la réaction Aider <mark>[voir la page 307 au chapitre 9]</mark>.

### Armures et compétences

Certaines armures imposent une pénalité sur certains tests de compétence et DD des tests opposés. Si une créature porte une armure qui impartit une pénalité de compétence, cette pénalité est appliquée à la Dextérité ainsi qu’aux tests de compétences et aux tests opposés basés sur la Force, à moins que l’utilisation de la compétence possède le trait attaque. <mark>[Voir les pénalités d’armure qui sont détaillées dans le chapitre 6 équipement à la page 176]</mark>

### Tests cachés

Dans certaines circonstances, vous ne saurez pas i votre test de compétence a réussi. Si l’utilisation d’une compétence possède le trait secret. Le MJ effectue le jet à votre place et vous informe de l’effet sans vous révéler le résultat du tirage ou le niveau de réussite. Le MJ effectue le tirage des jets secrets lorsque votre connaissance de l’issue est incertaine comme lorsque vous recherchez la présence d’une créature ou d’un objet caché ou lorsque vous tentez de tromper quelqu’un, de traduire un morceau d’un ancien texte plein de pièges ou de vous souvenir d’une ancienne légende. De cette manière, en tant que joueur, vous n’en savez pas plus que votre personnage. <mark>[Plus d’informations sur les tests cachés sont donnés au chapitre 9 en page 293]</mark>

## Activités de temps libres

Certaines compétences utilisent le trait temps libre. Ces activités ne peuvent être utilisées que lorsque vous disposez d’énormément de temps, habituellement une journée ou plus. Si vous n’êtes pas certain d’en avoir le temps pour en entreprendre une, demandez à votre MJ.

## Compétence de signature

Votre historique, votre classe et certaines capacités listent plusieurs compétences dans lesquelles vous êtes particulièrement efficaces ; ce sont les compétences de signature. Avec le temps, vous pouvez augmenter votre degré de formation dans ces compétences pour atteindre le degré de maître ou de légende. À l’inverse, vous êtes typiquement limité à un degré de formation d’expert au maximum dans les compétences qui ne sont pas des compétences de signature.

## Descriptions des compétences

Chacune des rubriques suivantes décrit une des compétences du jeu. Le titre de chaque rubrique donne le nom de la compétence, suivi par la Caractéristique liée à cette compétence entre parenthèses. Une brève description de la compétence est suivie par une liste des actions, des activités, des actions libres et des réactions que vous pouvez tenter si vous n’êtes pas formé dans cette compétence. Elles sont suivies des actions que vous ne pouvez accomplir que si vous êtes au moins entraîné dans cette compétence.

Comme les utilisations de ces compétences ne sont pas exhaustives, il y aura des cas où votre MJ vous demandera d’effectuer un test de compétence sans utiliser aucune des actions, des activités, des actions libres ou des réactions listées et des cas où il vous demandera de faire un jet en utilisant un autre modificateur de Caractéristique que celui indiqué.

{: .tabletitle}
Tableau des compétences, de leurs caractéristiques liées et leurs utilisations

{: .table .table-striped .table-sm .table-hover}
| Compétence     | Caractéristique liée                      | Utilisations sans entraînement          | utilisation entrainées                            |
| :------------- | ----------------------------------------- | --------------------------------------- | ------------------------------------------------: |
| **Acrobaties** | **Dextérité**                             | Avancer sans tomber (action)            | manoeuvre en vol (action)                         |
|                |                                           | Garder son équilibre (action libre)     |
|                |                                           | Se faufiler (activité)                  |
|                |                                           | Se libérer (action)                     |
|                |                                           | Se rattraper au bord (réaction)         |
|                |                                           | Traverser l'espace d'un ennemi (action) |
| **Arcanes**    | **Intelligence**                          | Se souvenir de connaissances (action)   | Apprendre un sort arcanique (activité)            |
|                |                                           |                                         | Emprunter un sort arcanique (activité)            |
|                |                                           |                                         | Identifier un objet/lieu/effet magique (activité) |
|                |                                           |                                         | Lire un écrit magique (activité)                  |
| **Athlétisme** | **Force**                                 | Agripper (action)                       | Désarmer (action)                                 |
|                |                                           | Escalader (action)                      |
|                |                                           | Forcer l'ouverture (action)             |
|                |                                           | Mettre à terre (action)                 |
|                |                                           | Nager (action)                          |
|                |                                           | Pousser (action)                        |
|                |                                           | Saut en hauteur (action)                |
|                |                                           | Saut en longueur (action)               |
|                |                                           | Se libérer d'une prise (action)         |
| **Artisanat**  | **Intelligence**                          | Réparer (activité)                      | Fabriquer (activité)                              |
|                |                                           |                                         | Identifier un objet alchimique (activité)         |
| **Tromperie**  | **Charisme**                              | Créer une diversion (action)            | Feinter (action)                                  |
|       |         | Mentir (activité)                         |
|        |        | Se faire passer pour quelqu'un (activité) |