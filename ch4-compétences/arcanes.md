---
title: "Arcanes (Intelligence)"
titleEN: "Arcana"
source: "Playtest Pathfinder"
toc: true

index:
  - key: "Arcanes (compétence)"
    anchor: ""
  - key: "Avancer sans tomber"
    anchor: "avancer-sans-tomber"
  - key: "Garder son équilibre"
    anchor: "garder-son-équilibre"
  - key: "se faufiler"
    anchor: "se-faufiler"
  - key: "se rattraper au bord"
    anchor: "se-rattraper-au-bord"
  - key: "traverser l'espace d'un ennemi"
    anchor: "traverser-l'espace-d'un-ennemi"
  - key: "manoeuvre en vol"
    anchor: "manoeuvre-en-vol"
---
# Arcanes (Intelligence)

Cette compétence mesure ce que vous connaissez de l’alchimie, des sorts arcaniques et des plus communs des objets magiques. 
De plus, elle détermine votre niveau d’information sur l’alchimie et les énergies arcaniques ainsi que le savoir sur les créatures qui leurs sont associés.
Si vous êtes entrainé en Arcanes, peu importe que vous soyez lanceur de sorts ou alchimiste, vous êtes versés dans l’art de reconnaître les objets alchimiques et magiques, même ceux que vous n’êtes pas capable d’utiliser.


## Utilisation de la compétence Arcanes sans entraînement

{: .block .action1}
### Se souvenir de connaissances
> 
> {: .traits}
>
> concentration
> secret
> 
> ---
> Vous tentez un test d’Arcanes pour essayer de vous remémorer un bout des connaissances concernant les réactions alchimiques, les théories arcaniques, les traditions magiques ou les créatures ayant une importance alchimique ou magique. Le MJ détermine les DD pour de tels tests.
>
> **Réussite** Vous vous souvenez de connaissances précises
>
> **Échec critique** Vous vous souvenez d’informations erronées

## Utilisations avec entrainement de la compétence Arcanes

Vous pouvez utiliser la compétence Arcanes selon les manières suivantes seulement si vous disposez d’un entraînement.

### Apprendre un sort arcanique
>
> Concentration
> 
> Si vous êtes un jeteur de sorts, vous pouvez disposer de l’accès à un nouveau sort par un autre jeteur de sorts qui connait ce sort ou à partir d’un parchemin ou d’un livre de sorts.
> 
> Pour apprendre le sort, vous devez procéder comme suit.
> - Dépenser une heure par niveau du sort, durant laquelle vous devez rester en conversation avec la personne qui connait le sort ou en possession de l’écrit magique.
> - Disposer d’un montant de matériaux magiques d’une valeur indiquée dans la table ci-dessus.
> - Essayer un test d’Arcanes avec un DD déterminé par le MJ (voir la table ci-dessus)
>
> **Réussite** Vous dépensez les matériaux et apprenez le sort. Si vous disposez d’un grimoire, le sort est ajouté à votre livre de sorts ; si vous préparez vos sorts à partir d’une liste, il est ajouté à cette liste ; si vous disposez d’un répertoire de sorts, vous pouvez le sélectionner lorsque vous pouvez échanger vos sorts ou lorsque vous en apprenez un nouveau.
> 
> **Réussite critique** Vous ne dépensez que la moitié du coût des matériaux.
>
> **Échec** Vous ne parvenez pas à apprendre le sort mais vous pourrez réessayer lorsque vous aurez acquis un niveau supplémentaire. Le coût des matériaux n’est pas dépenser.
>
> **Échec critique** Vous dépensez la moitié du coût des matériaux.

### Emprunter un sort arcanique
> 
> {: .traits}
>
> Concentration
> 
> ---
>
> Si vous êtes un jeteur de sort qui prépare ses sorts, durant vos préparatifs quotidiens, vous pouvez essayer de préparer un sort à partir du livre de sort de quelqu’un d’autre. Le MJ fixe le DD du test (voir le tableau suivant).
>
> **Réussite** Vous préparez le sort emprunté durant vos préparatifs quotidiens.
>
> **Échec** Vous échouez à préparer le sort mais l’emplacement du sort reste utilisable pour vous permettre de préparer un sort différent. Vous ne pouvez pas essayer de préparer ce sort avant la prochaine fois où vous effectuerez vos préparatifs.

### Identifier la magie
> 
> {: .traits}
>
> Concentration
> Secret
>
> ---
>
> Une fois que vous avez découvert qu’un objet, un effet en cours est magique, vous devez dépenser une heure à essayer d’identifier les particularités de sa magie. 
> Si votre tentative est interrompue, vous devez recommencer. Le MJ fixe le DD de votre test (voir le tableau suivant).
> Les sujets maudits ou ésotériques ont habituellement de plus haut DD ou peuvent même ne pas être identifiables en utilisant seulement la compétence Arcanes. Augmenter un sort ne rend pas plus difficile le DD nécessaire à son identification.
>
> **Réussite** Pour un objet ou un lieu, vous avez une idée de ce qu’il fait et de la manière de l’activer. Pour un effet en cours, vous obtenez le nom de l’effet et ce qu’il fait. Vous ne pouvez pas tenter de nouveau le test pour espérer une réussite critique.
>
> **Réussite critique** Vous apprenez tous les attributs de la magie incluant le fait de savoir s’il est maudit
>
> **Échec** Vous ne parvenez pas à identifier la magie et ne pouvez essayer pendant une journée
>
> **Échec critique** Vous confondez la magie avec autre chose au choix du MJ

{: .tabletitle}
Identifier ou apprendre un sort

{: .table .table-striped .table-sm .table-hover}
| Niveau du sort | Prix* | DD Typique*|
| :----- | ------ | ------:
| 1 ou sortilège | 20 pa | 13 |
| 2 | 60 pa | 16 |
| 3 | 160 pa | 19 |
| 4 | 360 pa | 22 |
| 5 | 700 pa | 25 |
| 6 | 1.400 pa | 28 |
| 7 | 3.030 pa | 31 |
| 8 | 6.500 pa | 34 |
| 9 | 15.000 pa | 37 |
| 10* | 70.000 pa | 40 |
{: .tablenotes}
(*) Les sorts rares du niveau 9 ou de niveau inférieur ont typiquement un prix et un DD correspondant à un sort d’un niveau de plus.

### Lecture de la magie
> 
> {: .traits}
>
> Concentration
> Secret
>
> ---
> 
> Vous pouvez lire et apprendre un écrit magique aussi longtemps qu’il est écrit dans une langue que vous pouvez normalement lire.
> 
> Cela prend habituellement environ une minute par page de texte mais cela pourrait prendre plus longtemps dans le cas d’écrits anciens obscurs ou dans d’autres situations à la discrétion du MJ.
> Pour lire et comprendre les nuances de textes particulièrement archaïques, ésotériques ou obscurs, le MJ peut vous demander d’effectuer un test d’Arcanes pour voir si vous parvenez à comprendre la véritable signification de l’écrit magique.
> Le MJ pourrait vouloir que vous tentiez un test par écrit ou que vous effectuiez plusieurs tests pour chaque partie d’un plus grand texte.
>
> **Réussite** Vous comprenez la véritable signification du texte
>
> **Échec critique** Vous vous trompez dans la lecture

