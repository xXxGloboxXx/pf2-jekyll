---
title: "Acrobaties (Dextérité)"
titleEN: "Acrobatics"
source: "Playtest Pathfinder"
toc: true

index:
  - key: "Acrobaties (compétence)"
    anchor: ""
  - key: "Avancer sans tomber"
    anchor: "avancer-sans-tomber"
  - key: "Garder son équilibre"
    anchor: "garder-son-équilibre"
  - key: "se faufiler"
    anchor: "se-faufiler"
  - key: "se rattraper au bord"
    anchor: "se-rattraper-au-bord"
  - key: "traverser l'espace d'un ennemi"
    anchor: "traverser-l'espace-d'un-ennemi"
  - key: "manœuvre en vol"
    anchor: "manœuvre-en-vol"
---

La compétence Acrobaties mesure votre capacité à accomplir des exploits physiques qui nécessitent de l’équilibre, de la coordination et de la grâce.

## Utilisations de la compétence Acrobaties sans entraînement

Les utilisations sans entrainement de la compétence sont les suivants :

{: .block .action1}
> ### Avancer sans tomber
> 
> {: .traits}
> Mouvement
> 
> ---
> 
> Vous pouvez vous déplacer sur une surface étroite ou un sol inégal en lançant un test d’Acrobaties contre le DD de la surface étroite ou du sol inégal.
> 
> Vous débutez cette action lorsque vous êtes sur la case qui contient la surface étroite, le sol inégal ou une surface similaire.
> 
> Vous êtes toujours pris au dépourvu lorsque vous vous trouvez sur une surface étroite ou un sol inégal.
> 
> **Réussite** Vous vous déplacez à votre Vitesse sur la surface étroite ou le sol inégal traitant le terrain comme un terrain difficile [voir page 312].
> 
> **Réussite critique** Vous vous déplacez à votre Vitesse sur la surface étroite ou le sol inégal traitant le terrain comme terrain un terrain normal.
> 
> **Échec** Vous devez rester sur cette case (gâchant votre action) ou vous chutez. Si vous chutez, votre tour se termine.
> 
> **Échec critique** Vous tombez et votre tour se termine.

{: .block .actionlibre}
> ### Garder son équilibre
> 
> **Déclencheur** Vous subissez des dégâts alors que vous vous trouvez sur une surface étroite ou un sol inégal ou que vous êtes en équilibre, tel que le MJ l’a déterminé.
> 
> ---
> 
> Vous essayez de conserver votre équilibre pour éviter la chute.
> 
> **Réussite critique** Vous parvenez à garder votre équilibre et n’avez plus besoin de faire de tentative pour ce round.
> 
> **Échec** Vous tombez.

{: .block}
> ### Se faufiler
> 
> {: .traits}
> Mouvement
> 
> ---
> 
> Vous essayez de vous faufiler dans un espace étroit en vous contorsionnant pour passer au travers.
> 
> **Réussite** Vous vous faufilez d’une case par minute.
> 
> **Réussite critique** Vous vous faufilez de 2 cases au travers de l’espace étroit.
> 
> **Échec critique** Vous restez coincé dans cet espace étroit. Tant que vous êtes coincé, vous devez passer une minute avant d’essayer un autre test d’Acrobaties contre le même DD. Tout autre résultat que celui d’un échec critique permet de vous décoincer.

{: .block .réaction}
> ### Se rattraper au bord
> 
> {: .traits}
> Manipulation
> 
> ---
> 
> **Déclencheur** Vous chutez à partir d’un bord ou lâchez prise.
> 
> **Conditions** Vos mains ne doivent ni être liées derrière votre dos ni autrement restreintes.
> 
> Lorsque vous chutez du bord ou basculez du bord ou par-dessus une rambarde, vous pouvez utiliser cette réaction pour tenter de vous rattraper et d’interrompre ainsi votre chute.
> Si vous vous rattrapez, vous pouvez essayer de vous rétablir en utilisant Escalade avec la compétence Athlétisme.
> 
> **Réussite** Si vous disposez d’au moins une main libre, vous vous rattrapez au bord où à la rambarde, stoppant votre chute. 
> Vous subissez toujours les dégâts selon la distance de chute jusque lors mais vous considérez que la hauteur de cette chute est de 6 m de moins qu’elle ne l’a été.
> Si vous ne disposez pas d’une main libre, vous poursuivez votre chute comme si vous ne vous étiez pas rattrapé.
> 
> **Réussite critique** Vous vous rattrapez au bord ou à la rambarde, que vous disposiez ou non d’une main libre, en utilisant typiquement un objet approprié tenu  pour vous rattraper (coinçant par exemple votre hache sur le rebord). 
> Vous subissez toujours les dégâts selon la distance de chute jusque lors mais vous considérez que la hauteur de cette chute est de 9 m de moins qu’elle ne l’a été.
> 
> **Échec critique** Vous poursuivez votre chute et si vous êtes tombé d’une distance supérieure à 6 m avant d’utiliser cette réaction, vous subissez 10 points de dégâts contondants pour chaque tranche de chute de 6 m lors de l’impact.

{: .block .action1}
> ### Traverser l'espace d'un ennemi
>
> {: .traits}
> Mouvement
>
>---
>
>Vous vous déplacez à votre Vitesse. Au cours de ce mouvement, vous pouvez passer par l’espace d’un adversaire. 
> Vous devez faire un test d’Acrobaties contre le DD de Réflexes de votre ennemi dès que vous entrez dans son espace.
>Vous pouvez passer à travers l’espace de votre ennemi, en utilisant Escalade, Vol, Nager ou tout autre type de déplacement approprié à l’environnement.
>
> **Réussite** Vous traversez l’espace de votre ennemi, en considérant les cases dans son espace comme un terrain difficile.
> Si votre Vitesse ne vous permet pas d’atteindre une case hors de son espace, vous obtenez le même résultat qu’en cas d’échec.
>
> **Échec** Votre déplacement se termine et vous déclenchez les mêmes réactions (comme Attaque d’opportunité) que si vous vous étiez sorti de la case par laquelle vous avez entamé votre mouvement.

## Utilisations de la compétence Acrobaties avec entraînement
Vous ne pouvez utiliser l’action suivante que si vous êtes entraîné dans cette compétence :

{: .block .action1}
> ### Manœuvre en vol
> {: .traits}
> Mouvement
>
> ---
> Si vous disposez d’une Vitesse de vol [voir page 310] et que vous devez accomplir une manœuvre difficile alors que vous volez, tentez un test d’Acrobaties.
>
> **Réussite** Vous réussissez la manœuvre
>
> **Réussite critique** Vous parvenez à accomplir la manœuvre et obtenez un bonus de circonstances à vos tests de Manœuvre en Vol jusqu’à la fin de votre prochain tour.
>
> **Échec** Votre manœuvre échoue et le MJ choisit si vous ne pouvez pas vous déplacer ou si un autre effet déterminé survient. 
> Cette conséquence doit être aussi appropriée à la manœuvre que vous venez de tenter (par exemple, être dévié de votre route si vous luttiez contre un vent fort).
