---
title: "Artisanat (Intelligence)"
titleEN: "Craft"
source: "Playtest Pathfinder"
toc: true

index:
  - key: "Artisanat (compétence)"
    anchor: ""
  - key: "Réparer"
    anchor: "réparer"
  - key: "Fabriquer"
    anchor: "fabriquer"
---

Vous pouvez utiliser cette compétence pour créer et réparer des objets.

## Utilisation de la compétence Artisanat sans entraînement
Vous pouvez utiliser la compétence Artisanat sans entraînement de la manière suivante :

> ### Réparer
>
> {: .traits}
> Manipulation
>
> ---
> **Conditions** Vous devez utiliser un nécessaire de réparation (voir page 187).
>
> ---
> 
> Vous passez une heure à essayer de réparer un objet (abimé) ou (brisé). 
> Le MJ définit le DD mais il est souvent plus difficile de réparer un objet magique ou de qualité supérieure. 
> Vous ne pouvez pas réparer un objet détruit.
>
> **Réussite** Vous enlevez un (coup) porté à l’objet. Dans la plupart des cas, cela suffit à réparer l’objet abimé et un objet brisé qui est réparé devient abimé.
>
> **Réussite critique** Vous réparez intégralement l’objet.
>
> **Échec critique** Vous abimez l’objet. Si l’objet est magique, ce coup ne peut pas le détruire.

## Utilisation de la compétence Artisanat avec entraînement
Vous pouvez tenter l’utilisation de la compétence Artisanat suivante seulement si vous êtes entraîné dans cette compétence

> ### Fabriquer
>
> {: .traits}
> Manipulation
> Temps libre
> 
> ----
> 
> Vous pouvez fabriquer un objet à partir des matériaux bruts. Le MJ détermine le DD nécessaire à la fabrication d’un objet qui est fonction de son niveau, de sa qualité et de sa rareté et pourrait être affecté par d’autres circonstances.
> Vous avez besoin du don Artisanat alchimique (page 162) pour créer des objets alchimiques, du don Artisanat magique pour créer des objets magiques (page 168) et du don Fabrication de trappes (page 171) pour créer des pièges.
>
> Pour fabriquer un objet, vous devez remplir les conditions suivantes :
> -	Cet objet doit être de votre niveau ou inférieur (Un objet sur lequel ne figure aucun niveau est de niveau 0),
> -	Vous devez avoir le degré de formation approprié à la qualité de l’objet (voir p. 190),
> -	Vous disposez de la formule,
> -	Vous avez un jeu d’outils approprié et dans la plupart des cas accès à un atelier. Par exemple, vous avez accès à une forge pour forger un bouclier en métal.
> -	Vous devez fournir les matériaux bruts d’une valeur égale à la moitié du Prix de l’objet. Vous dépensez toujours au moins ce montant lorsque vous réussissez à Fabriquer un objet. Si vous êtes dans un lieu de peuplement, vous pouvez habituellement dépenser ce montant en monnaie pour obtenir l’équivalent des matériaux bruts dont vous avez besoin, à l’exception des matériaux spéciaux les plus rares.
> Vous avez besoin de dépenser un minimum de jours de temps libre pour parvenir à un stade basique pour obtenir un objet complet lors de la Fabrication d’un objet.
> Ce nombre dépend de votre niveau et du niveau de l’objet. Vous avez besoin de dépenser quatre jours de temps libre pour un objet de votre niveau. Réduisez le nombre de jour d’1 pour chaque niveau que vous possédez au-delà du niveau de l’objet avec un minimum de 1.
> Après avoir passé ce nombre de jours, vous pouvez soit dépenser plus de matériaux pour compléter l’objet immédiatement ou passer plus de jours à la Fabrication, complétant ainsi plus lentement  mais sans coût supplémentaire.
> La ligne Réussite explique ce processus plus complètement.
> Vous pouvez fabriquer les objets avec le trait utilisation unique par groupe, en fabriquant jusqu’à quatre de la même sorte simultanément.
> Cela nécessite d’inclure les matériaux bruts pour tous les objets composant le lor au départ et vous devez terminer le lot en une fois.
> Après avoir dépensé le montant requis de matériaux bruts et la base de temps libre, tentez un test de Fabrication. Si vous échouez ou échouez de manière critique, vous pouvez essayer de fabriquer de nouveau l’objet, mais vous devez recommencer du départ.
>
> **Réussite** Votre essai de création de l’objet est un succès. Vous pouvez payer la moitié restant du Prix de l’objet pour le terminer immédiatement ou dépenser du temps libre supplémentaire pour y parvenir. Pour chaque jour supplémentaire que vous y passez, vous réduisez la valeur en matériaux bruts dont vous avez besoin pour terminer l’objet par un montant basé sur votre niveau et votre degré de formation (voir la table ci-après). 
> Après chacun de ces jours de temps libre, vous pouvez terminer l’objet en dépensant le montant restant des matériaux nécessaires. Si votre temps libre est interrompu, vous pouvez revenir pour terminer l’objet par la suite, en reprenant précisément là où vous en étiez.
>
> **Réussite critique** Comme pour la réussite sauf que chaque jour passé à réduire le coût des matériaux restants compte comme si vous aviez un niveau de plus.
> 
> **Échec** Vous échouez à réaliser l’objet. Vous pouvez récupérer les matériaux bruts que vous avez apportés pour la totalité de leur valeur.
>
> **Échec critique** Vous échouez à terminer l’objet. Vous gâchez 10% des matériaux bruts que vous avez fournis mais pouvez récupérer le reste.

{: .tabletitle}
Progression de la fabrication par jour

{: .table .table-striped .table-sm .table-hover}
| Votre niveau | Entraîné | Expert | Maître | Légendaire
| 1 | 1 pa | — | — | —
| 2 | 2 pa | — | — | —
| 3 | 4 pa | 4 pa | — | —
| 4 | 5 pa | 6 pa | — | —
| 5 | 9 pa | 10 pa | — | —
| 6 | 12 pa | 14 pa | — | —
| 7 | 16 pa | 20 pa | 20 pa | —
| 8 | 20 pa | 28 pa | 30 pa | —
| 9 | 25 pa | 36 pa | 40 pa | —
| 10 | 30 pa | 45 pa | 50 pa | —
| 11 | 35 pa | 60 pa | 75 pa | —
| 12 | 40 pa | 75 pa |100 pa | —
| 13 | 50 pa | 100 pa | 150 pa | —
| 14 | 75 pa | 150 pa | 250 pa | —
| 15 | 100 pa | 200 pa | 350 pa | 350 pa
| 16 | 125 pa | 250 pa | 475 pa | 500 pa
| 17 | 150 pa | 300 pa | 650 pa | 700 pa
| 18 | 200 pa | 450 pa | 900 pa | 1.000 pa
| 19 | 300 pa | 600 pa | 1.200 pa | 1.500 pa
| 20 | 375 pa | 750 pa | 1.500 pa | 2.000 pa
| 20 (crit.) | 450 pa | 900 pa | 1.750 pa | 2.500 pa
