---
title: "Diplomatie (Charisme)"
titleEN: "Diplomacy"
source: "Playtest Pathfinder"
toc: true

index:
  - key: "Diplomatie (compétence)"
    anchor: ""
  - key: "Amadouer"
    anchor: "amadouer"
  - key: "Formuler une requête"
    anchor: "formuler-une-requête"
  - key: "Recueillir des informations"
    anchor: "recueillir-des-informations"
  
---

Diplomatie (Charisme)
Vous influencez les autres par la négociation et la flatterie.

Utilisations de la compétence Diplomatie sans entraînement
Voici les utilisations de la compétence Diplomatie que vous pouvez accomplir sans entraînement

> ### Amadouer
>
> {: .traits}
> Auditif
> Concentration
> Langage
> Mental
> Temps libre
> Secret
> 
>---
>
> Après au moins une minute de conversation consistant en des ouvertures charismatiques, des flatteries et d'autres manifestations de bonne volonté, vous cherchez à faire bonne impression sur quelqu'un pour le rendre temporairement agréable.
> Vous cherchez à faire une bonne impression sur quelqu’un pour le rendre temporairement agréable. Au terme de la conversation, faites un test de Diplomatie contre le DD de Volonté de la cible modifié par les circonstances que le MJ pense adaptées.
> Les bonnes impressions (ou les mauvaises, sur une réussite critique) durent pendant la rencontre actuelle seulement à moins que le MJ en décide autrement.
> Les attitudes sont listées dans la partie Conditions (voir page 319). La tentative échoue si la cible ne peut pas vous comprendre.
>
> **Réussite** L’attitude de la cible envers vous s’améliore d’un cran (d’hostile à inamicale, d’inamicale à indifférente, d’indifférente à amicale, d’amicale à serviable).
> **Réussite critique** L’attitude de la cible envers vous s’améliore de deux crans (d’hostile à indifférente, d’inamicale à amicale, d’indifférente à serviable, d’amicale à serviable).
> **Échec critique** L’attitude de la cible envers vous se dégrade d’un cran (de serviable à amicale, d’amicale à indifférente, d’indifférente à inamicale, d’inamicale à hostile).

> {: .action1}
> ### Formuler une requête
>
> {: .traits}
> Auditif
> Concentration
> Langage
> Mental
>
> ---
> Vous pouvez formuler une requête à une créature qui est amicale ou serviable avec vous. Vous devez formuler la requête dans des termes que la cible acceptera compte tenu de son attitude actuelle envers vous. Le MJ détermine le test en fonction de la difficulté de la requête. Certaines requêtes sont désagréables ou impossible et même un PNJ serviable ne les acceptera jamais.
>
> **Réussite** La cible accepte votre requête mais peut demander d’ajouter des dispositions ou des modifications à la requête.
> **Réussite critique** La cible accepte votre requête sans réserves ou accepte une requête qui aurait ordinairement nécessité d’avoir une attitude d’un cran plus favorable à votre égard.
> **Échec** La cible refuse la requête, bien qu’elle puisse proposer une alternative qui soit moins extrême.
> **Échec critique** Non seulement votre cible refuse la requête mais son attitude se dégrade d’un cran à votre encontre à cause de la témérité de la requête.

> ### Recueillir des informations
> Vous fréquentez les marchés locaux, les tavernes et les lieux de rassemblement pour tenter d’en apprendre davantage sur un individu ou un objet spécifique.
> Le MJ détermine le DD du test et le temps nécessaire (Typiquement, si vous utilisez une journée complète de temps libre, vous pouvez faire 3 à 4 tests) ainsi que les bénéfices que vous pouvez recueillir en dépensant quelques pièces pour payer une tournée, faire des cadeaux ou corrompre.
>
> **Réussite** Vous parvenez à collecter des informations sur un individu ou un sujet spécifique. Le MJ détermine les détails.
>
> **Échec critique** Vous collectez des informations incorrectes sur le sujet ou l’individu.

