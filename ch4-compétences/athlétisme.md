---
title: "Athlétisme (Force)"
titleEN: "Athletics"
source: "Playtest Pathfinder"
toc: true

index:
  - key: "Athlétisme (compétence)"
    anchor: ""
  - key: "Agripper"
    anchor: "agripper"
  - key: "Escalader"
    anchor: "escalader"
  - key: "Forcer l'ouverture"
    anchor: "forcer-louverture"
  - key: "Mettre à terre"
    anchor: "mettre-à-terre"
  - key: "Nager"
    anchor: "nager"
  - key: "Pousser"
    anchor: "pousser"
  - key: "Sauter en hauteur"
    anchor: "sauter-en-hauteur"
  - key: "Sauter en longueur"
    anchor: "sauter-en-longueur"
  - key: "Se libérer d'une prise"
    anchor: "se-libérer-dune-prise"
  - key: "Désarmer"
    anchor: "désarmer"

---

L’entraînement en Athlétisme est le résultat du conditionnement qui vous autorise à accomplir des prouesses physiques.

## Utilisations sans entraînement
Les utilisations de la compétence sans entraînement sont les suivants :

{: .block .action1}
> ### Agripper (action)
> 
> {: .traits}
> Attaque
>
> ---
> 
> **Conditions** Vous devez avoir au moins une main libre.
> Votre cible ne doit pas avoir plus de deux tailles de plus que vous.
> 
> ---
> Vous essayez d’agripper un adversaire. Agripper nécessite de réaliser un test d’Athlétisme contre le DD de Vigueur de votre adversaire.
>
> **Réussite** Votre adversaire est agrippé jusqu’à la fin de votre prochain tour à moins que vous ne fassiez mouvement ou que votre adversaire échappe à votre prise.
>
> **Réussite critique** Votre adversaire est entravé jusqu’à la fin de votre prochain tour à moins que vous ne fassiez mouvement ou que votre adversaire ne se libère.
>
> **Échec** Vous ne parvenez pas à agripper votre adversaire. Si vous l’aviez déjà agrippé ou entravé par le biais de l’action d’agripper, ces conditions cessent sur cette créature.
>
> **Échec critique** Si vous aviez déjà agrippé ou entravé votre adversaire, celui-ci est libre. Votre cible peut vous agripper comme si elle était parvenue à réaliser l’action agripper contre vous ou vous forcer à tomber et vous mettre à terre.

{: .block .action1}
> ### Escalader
>
> {: .traits}
> Mouvement
> 
> ---
>
> **Conditions** Vous devez avoir les deux mains libres. 
>
> ---
> 
> Vous pouvez monter, descendre ou franchir une pente.
> À moins que l’escalade soit particulièrement facile, vous devez essayer un test d’Athlétisme. Le DD est déterminé par le MJ en fonction de la nature de la pente et des circonstances environnementales. 
> Si vous tombez, l’utilisation de la compétence Acrobaties pour se rattraper au bord pourrait vous permettre de vous rattraper.
> Vous êtes pris au dépourvu lorsque vous escaladez à moins d’avoir une vitesse d’escalade.
>
> **Réussite** Vous vous déplacez d’une case vers le haut, le bas ou à travers la pente. Si votre vitesse atteint ou dépasse 8 cases, vous vous déplacez de 2 cases.
>
> **Réussite critique** Vous vous déplacez à la moitié de votre vitesse vers le haut, le bas ou à travers la pente.
>
> **Échec critique** Vous tombez [voir page 310]. Si vous avez commencé l’escalade sur un sol stable, vous tombez et atterrissez à terre.

{: .block .action1}
> ### Forcer l'ouverture
> 
> {: .traits}
> attaque
> 
> ---
>
> Utilisant votre corps, un levier ou un autre outil, vous essayez d’ouvrir de force une porte, une fenêtre ou un récipient, lever une lourde porte ou forcer l’ouverture de liens qui peuvent entraver votre mobilité.
> Avec un résultat assez élevé, vous pouvez même fracasser les murs.
>
> **Réussite** Vous forcez l’ouverture de la porte, de la fenêtre ou du récipient et la porte, la fenêtre ou le récipient obtient la condition brisé.
>
> **Réussite critique** Vous forcez l’ouverture de la porte, de la fenêtre ou du récipient mais pouvez éviter de l’endommager dans le processus
>
> **Échec critique** Votre tentative fausse la porte, la fenêtre ou le récipient, imposant une pénalité de circonstance de -2 à toutes les futures tentatives de l’ouvrir.

{: .block .action1}
> ### Mettre à terre
> 
> {: .traits}
> Attaque
>
> ---
> 
> **Conditions** Vous devez avoir au moins une main libre.
> Votre cible ne doit pas avoir plus de deux tailles de plus que vous.
> 
> ---
> 
> Vous essayez de mettre à terre un adversaire.
> Tentez un test d’Athlétisme contre le DD de Réflexes de votre adversaire.
>
> **Réussite** La cible tombe à terre.
>
> **Réussite critique** La cible tombe à terre et subit 1d6 dégâts contondants.
>
> **Échec critique** Vous perdez l’équilibre et chutez pour vous retrouver à terre. 

{: .block .action1}
> ### Nager
> 
> {: .traits}
> Mouvement
> 
> ---
> Vous utilisez vos bras et vos jambes pour nager dans l’eau.
> Dans la plupart des eaux calmes, vous réussissez cette action sans devoir réaliser un test. Si vous devez respirer de l’air mais que vous êtes submergé sous l’eau, vous devez retenir votre souffle chaque round. Si vous n’y parvenez par, vous commencez à vous noyer [comme indiqué à la page 315].
> Si l’eau dans laquelle vous vous trouvez est agitée ou autrement dangereuse, vous devrez réaliser un test d’Athlétisme pour nager.
> Si vous finissez votre tour dans l’eau et n’êtes pas parvenu à réaliser l’action de nager durant ce tour, vous coulez de 3 m, comme déterminé par le MJ. Cependant, si la dernière action de votre tour était de rentrer dans l’eau, vous ne coulez pas et ne flottez pas ce tour.
> 
> **Réussite** Vous vous déplacez d’une case horizontalement dans l’eau. Si votre vitesse est au moins de 3 cases, vous vous déplacez de 2 cases et si votre vitesse est de 12 cases, vous vous déplacez de 3 cases. Vous pouvez nager vers le haut ou vers le bas mais cela compte alors comme un déplacement à travers un terrain difficile.
> 
> **Réussite critique** Comme pour une réussite mais vous vous déplacez d’une case supplémentaire.
> 
> **Échec critique** Vous ne faites aucun progrès et cette action compte comme deux actions lorsqu’il s’agit de retenir votre souffle.

{: .block .action1}
> ### Pousser
> 
> {: .traits}
> Attaque
>
> ---
> 
> **Conditions** Vous devez avoir au moins une main libre.
> Votre cible ne doit pas avoir plus de deux tailles de plus que vous.
> 
> ---
> Vous repoussez un adversaire loin de vous.
Faites un test d’Athlétisme contre le DD de Vigueur de votre adversaire.
>
> **Réussite** Vous repoussez votre adversaire d’une case dans la même direction que celle dont vient votre adversaire. Si vous réalisez cette action, elle obtient le trait mouvement.
> 
> **Réussite critique** Vous repoussez votre adversaire de deux cases et vous pouvez vous déplacer de deux cases dans la même direction que ce dernier. Si vous le faites, cette action prend le trait mouvement.
> 
> **Échec critique** Vous perdez l’équilibre et chutez pour vous retrouver à terre. 

{: .block .action1}
> ### Sauter en hauteur
> 
> Vous prenez de l’élan et si vous avez parcouru au moins 2 cases, lancez un test d’Athlétisme contre un DD30 pour tenter un saut vertical. 
> Ce DD peut être augmenté ou diminué en fonction de la situation, tel que déterminé par le MJ.
>
> **Réussite** Vous sautez et augmentez la distance verticale de 1,5 m.
> 
> **Réussite critique** Vous sautez et soit vous augmentez la distance verticale à 2,10 m soit vous augmentez la distance verticale de 1,5 m et la distance horizontale de 2 cases.
>
> **Échec** Vous sautez de la hauteur maximale permise par l’action Sauter.
>
> **Échec critique** Au lieu de sauter, vous tombez à terre dans votre case.

{: .block .action1}
> ### Sauter en longueur
> 
> Vous prenez de l’élan et si vous avez parcouru au moins 2 cases, faites un test d’Athlétisme pour essayer de sauter horizontalement dans la même direction. Le DD du test est égal à 5 plus la distance totale en incrément de 33,33 cm que vous essayez de franchir durant votre saut (ainsi sauter 4 cases nécessitera que vous battiez un DD 25) 
> 
> Vous ne pouvez sauter plus que votre Vitesse.
Ce DD peut être augmenté ou diminué en fonction de la situation, tel que déterminé par le MJ.
>
> **Réussite** Vous parvenez à sauter et à franchir la distance horizontalement jusqu’à la distance désirée.
>
> **Échec** Vous sautez de la distance maximale obtenue par l’action de sauter.
>
> **Échec critique** Vous sautez de la distance maximale obtenue par l’action de sauter, avant de tomber à terre.

{: .block .action1}
> ### Se libérer d'une prise
>
> Vous tentez de vous libérer de la prise d’un adversaire avec un test d’Athlétisme contre le DD de cette créature.
>
> **Réussite** Vous vous libérez de la prise de votre ennemi et retirer les conditions agrippé ou entravé infligées par ce dernier.
>
> **Réussite critique** Vous retirez les conditions agrippé ou entravé infligées par votre ennemi et vous pouvez soit mettre à terre votre adversaire ou l’agripper comme si vous aviez réussi à l’agripper. Si vous agrippez l’adversaire, cette action obtient le trait attaque.
>
> **Échec critique** Si vous étiez agrippé, vous devenez entravé. Si vous étiez déjà entravé, vous ne pouvez pas essayer de vous libérer de la prise jusqu’au début de votre prochain tour.

## Utilisation de la compétence avec entraînement
Vous pouvez accomplir l’utilisation suivante d’Athlétisme seulement si vous êtes au moins entraîné dans cette compétence.

{: .block .action1}
> ### Désarmer (action)
> 
> {: .traits}
> Attaque
>
> ---
> 
> **Conditions** Vous devez avoir au moins une main libre.
> Votre cible ne doit pas avoir plus de deux tailles de plus que vous.
> 
> ---
> 
> Vous essayez de faire lâcher à un adversaire ce qu’il tient. 
> Un désarmement nécessite que vous fassiez un test d’Athlétisme contre le DD de Réflexes de votre adversaire.
>
> **Réussite** Vous affaiblissez la prise de votre adversaire sur l’objet qu’il tient. Jusqu’au début de votre prochain tour, les essais pour désarmer l’adversaire se font avec un bonus de circonstances de +2.
>
> **Réussite critique** Vous faites lâcher l’objet à votre adversaire qui tombe sur le sol dans sa case.
>
> **Échec critique** Vous perdez l’équilibre et êtes pris au dépourvu jusqu’au début de votre prochain tour.
