---
title: "Tromperie (Charisme)"
titleEN: "Deception"
source: "Playtest Pathfinder"
toc: true

index:
  - key: "Tromperie (compétence)"
    anchor: ""
  - key: "Mentir"
    anchor: "mentir"
  - key: "Créer une diversion"
    anchor: "créer-une-diversion"
  - key: "Se faire passer pour quelqu'un d'autre"
    anchor: "Se faire passer pour quelquun d'autre"
  - key: "Feinter"
    anchor: "feinter"
  
---

Vous pouvez tricher et embobiner les autres au moyen de déguisements, de mensonges et différentes formes de subterfuge

## Utilisations de la compétence Tromperie sans entraînement
Vous pouvez utiliser la compétence Tromperie sans entraînement des manières suivantes :

{: .block .action1}
> ###  Créer une diversion
> 
> D’un geste, d’un tour ou par des mots destinés à créer une distraction, vous pouvez créer une diversion qui attire l’attention de la créature autre part.
> Si vous utilisez un geste ou un tour, la diversion prend le trait (manipulation). Si vous utilisez des mots, la diversion prend les traits (auditif) et (langage).
> Tentez un unique test de Tromperie et comparez-le au DD de (Perception) des créatures dont vous cherchez à distraire l’attention. Que vous réussissiez ou pas, les créatures que vous essayez d’affecter obtiennent un bonus de circonstance de +4 à leur DD de Perception contre les tentatives de diversion pendant une minute.
>
> **Réussite** Vous êtes simplement détecté au lieu d’être vu au regard de l’attention de chaque créature dont vous parvenez à battre le DD. Cela dure jusqu’à la fin de votre tour ou jusqu’à ce que vous ayez agi pour faire autre chose que Step ou réaliser les actions (Se cacher) ou (Se déplacer furtivement) de la compétence (Discrétion). Si vous faites autre chose, la condition détecté cesse juste avant que vous n’agissiez. Par exemple si vous réussissez un test de Tromperie, devenant momentanément (détecté) par une créature (au lieu d’être vu) et que vous attaquez, elle n’est pas (prise au dépourvu) par votre attaque.
>
> **Échec** Vous ne parvenez pas à distraire l’attention de chaque créature dont vous ne parvenez pas à battre le DD de (Perception) et ces créatures sont conscientes que vous avez essayé de les tromper.

> ### Mentir
>
> {: .traits} 
>
> Auditif
> Concentration
> Mental
> Secret
>
> ---
>
> Vous essayez de tromper quelqu’un avec une contre-vérité. Faire ainsi vous prend au moins un tour ou plus longtemps si le mensonge est élaboré.
> Vous lancez un test de Tromperie et le comparez au DD de Perception de chaque créatue que vous essayez de tromper. Le MJ peut modifier ce DD en fonction de la situation et de la nature du mensonge que vous êtes en train de raconter.
> Les mensonges élaborés ou hautement incroyables sont beaucoup plus difficiles à faire gober à une créature que des mensonges plus simples et plus crédibles et certains mensonges sont si gros qu’il est impossible d’amener quiconque à les croire.
> À la discrétion du MJ, si une créature commence à vous croire, elle peut toujours être capable de tenter un test de Perception contre votre DD de Tromperie pour réaliser que c’est un mensonge (la réussite sur cette créature obtient le même effet qu’un échec). Cela se produit habituellement si la créature en découvre assez de preuves pour contrer vos affirmations.
> 
> **Réussite** La cible croit à votre mensonge.
> 
> **Échec** La cible ne vous croit pas et obtient un bonus de circonstance de +4 aux tentatives ultérieures de lui mentir pendant la durée de votre conversation. La cible est aussi plus facilement suspicieuse à votre encontre pour l’avenir.
> 

> ### Se faire passer pour quelqu’un
>
> Vous créez un déguisement pour vous faire passer pour quelqu’un ou quelque chose que vous n’êtes pas. Assembler des morceaux d’un déguisement décent vous prend 10 minutes et nécessite un kit de déguisement mais un déguisement plus simple, plus rapide peut faire l’affaire si vous n’essayez pas d’imiter un individu spécifique à la discrétion du MJ.
> Dans la plupart des cas, les créatures ont une chance de vous détecter seulement s’ils utilisent l’action de Rechercher pour tenter un test de Perception contre le DD de votre Tromperie. 
> Si vous essayez d’interagir directement avec quelqu’un lorsque vous êtes déguisé, vous avez besoin de faire un test secret de Tromperie contre le DD de Perception de la créature à la place.
> Si vous êtes déguisé pour remplacer quelqu’un de particulier, le MJ peut donner aux créatures qui interagissent avec un bonus qui dépend de leur degré d’intimité avec la personne que vous tentez d’imiter ou vous demander un test secret de Tromperie même si vous n’interagissez pas directement avec elles.
>
> **Réussite** Vous trompez la créature et lui faites croire que vous êtes la personne pour laquelle vous vous faites passer. Vous pouvez avoir à tenter un nouveau test si votre comportement change.
>
> **Échec** La créature peut dire que vous n’êtes pas celui que vous prétendez être.

## Utilisations de la compétence Tromperie avec entraînement
Vous ne pouvez utiliser la compétence Tromperie de la manière suivante que si vous y êtes entrainé :

{: .block .action1}
> ### Feinter
>
> {: .traits} 
> Mental
> 
> ---
> 
> Condition Vous êtes à portée d’allonge de la cible
> 
> ---
> D’un mouvement trompeur, vous laissez votre adversaire non préparé pour votre attaque réelle. 
> Vous tentez un test de Tromperie contre le DD de Perception de votre adversaire.
>
> **Réussite** La cible est (prise au dépourvu) contre votre prochaine attaque de mêlée que vous tenterez à son encontre avant la fin de votre prochain tour.
> 
> **Réussite critique** La cible est prise au dépourvu contre toutes les attaques de mêlée que vous tenterez à son encontre avant la fin de votre prochain tour.
> 
> **Échec critique** Vous êtes pris au dépourvu contre les attaques de mêlée portées à votre encontre jusqu’à la fin de votre prochain tour.
