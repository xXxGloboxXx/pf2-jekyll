---
title: "Gérer les modes de jeu"
source: "Playtest Pathfinder"
toc: true
---

Comme indiqué dans le chapitre [Jouer à Pathfinder](/ch9-jouer-à-pathfinder/jouer-à-pathfinder.html), les parties de Pathfinder peuvent se décomposer en trois types d'activités ou modes de jeu.

Le mode "rencontres" se déroule en temps réel ou plus lentement encore et décrit des conflits directs entre des joueurs et des ennemis ou des alliés potentiels. Les combats et les interactions sociales se passent généralement dans ce mode de jeu.

Le "liant" d'une aventure, le mode d'exploration, est utilisé quand les personnages explorent un emplacement où pourrait se cacher des danges, comme une ville inconnue ou un donjon. En mode exploration, les personnages ne sont pas en dangers immédiats mais ils doivent tout de même être sur leurs gardes. L'exploration et les rencontres, ensemble, forment la partie "aventure".

Quand les personnages ne sont pas en aventure, le groupe est en "temps libre". Ce mode recouvre la plupart des activités quotidiennes d'une personne normale, comme les tâches habituelles et normales ou encore les efforts fournis pour des objectifs à plus long terme.

## Les rencontres

Comme le mode de rencontre est le mode de jeu le plus précisément régi par des règles, vous vous contentrez souent de suivre les règles du chapitre [Jouer à Pathfinder](/ch9-jouer-à-pathfinder/jouer-à-pathfinder.html). Comme, bien souvent, vous demanderez à ce qu'on déterminer l'initiative pendant le mode d'exploration, juste avant de passer au mode rencontre, les conseils pour déterminer l'ordre d'initiative sont présentés [plus loin](#lancer-linitiative) dans la section dédiée au mode exploration. Les règles pour construire des rencontres de combat équilibrées se trouvent dans le [[BESTIAIRE]].

{: .tabletitle}
Mode rencontre

<table class="table">
<tbody>
<tr>
  <th>Enjeux</th>
  <td>Modérés à élevés. Les rencontres ont toujours des enjeux significatifs, ce qui explique qu'elles se déroulent à vitesse réduite.</td>
</tr>
<tr>
  <th>Échelle de temps</th>
  <td>En mode rencontre, le temps passe à un rythme bien précis, au gré des rounds de combats pour les rencontres de combats et des rounds appropriés pour les autres types de rencontres. En combat, 1 minute correspond à 10 rounds, de sorte qu'un round dure 6 secondes mais vous pourriez par exemple décider qu'un duel verbal se déroule en rounds durant 1 minute chacun pour donner suffisamment de temps à chacun des intervenants qui s'expriment.</td>
</tr>
<tr>
  <th>Actions et réactions</th>
  <td>Les rencontres de combat se divisent en actions séparées et les personnage peuvent utiliser des réactions lorsque leur déclencheur se produit. Des réactions peuvent également se dérouler au cours des situations sociales mais leur déclencheur sont généralement plus descriptifs et moins tactiques, ce qui vous donne, à vous et aux autres joueurs, plus de flexibilité.</td>
</tr>
</tbody>
</table>

### Choisir les actions des adversaires

Bien souvent, les joueurs se coordonnent et plannifient pour être aussi efficaces que possible, mais leurs adversaires ne le font pas toujours. En tant que MJ, vous jouez le rôle de ces ennemis, ce qui consiste entre autres à décider de leur tactique. La plupart des créatures n'ont qu'une vague idée des tactique telles que la prise en tenaille ou le fait de se concentrer sur une seule cible. Cependant, vous devriez vous souvenir qu'ils ont des réactions émotionnelles et qu'ils font des erreurs, encore plus que les personnages joueurs.

Utilisez ce que les adversaires savent de la situation pour choisir leur cibles ou les capacités qu'ils vont utiliser, pas ce que vous en savez. Vous pourriez savoir que le prêtre a un modificateur de Volonté très élevé mais un monstre pourrait tout de même tenter d'utiliser un effet de peur sur lui. Ça ne veut pas dire pour autant que vous devriez jouer les ennemis comme de complets idiots. Ils peuvent apprendre de leurs erreurs et concevoir des plans solides, et les ennemis plus intelligents pourraient enquêter pour en svaoir plus sur les personnages joueurs à l'avance.

Les adversaires arrêtent généralement d'attaquer quelqu'un qui est hors combat. Même si une créature sait qu'un ennemi inconscient pourriat revenir au combat, seules les plus vicieuses d'entre elles se concentrent sur des ennemis sans défense plutôt que sur les menaces plus immnentes qui les entourrent.

Interpréter des ennemis est un mélange entre rester fidèle à la créature en question et faire les choix qui sont les meilleurs pour l'aspect dramatique du jeu. Voyez la chose comme un film ou une scène de combat dans un roman. Si le guerrier insulte un géant du Feu pour attirer son attention et l'éloigner du fragile magicien, il se pourrait bien que la meilleure décision tactique pour le géant soit de continuer à réduire le magicien en purée. Demandez-vous si c'est ce choix qui donnera la meilleure scène, ou si vous préféreriez une scène où le géant redirige sa colère vers l'énervant guerrier.

### Mettre un terme à une rencontre

Une rencontre de combat se termine généralement quand toutes les créatures d'un camp ont été tuées ou rendues inconscientes. Quand cela s'est produit, vous pouvez arrêter d'agir en ordre d'initiative. Le camp vainqueur a ensuite généralement tout son temps pour s'assurer que toutes les créatures mises hors combat ne se relèvent pas. Il pourrait être nécessaire de continuer à utiliser des rounds de rencontre si certains membres du camp vainqueur sont proches de la mort, accrochés au bord d'un précipice ou dans une autre situation où chaque seconde compte et leur survie est en question.

Vous pouvez considérer qu'un combat est terminé quand il ne reste plus de difficulté et que les personnage joueurs sont juste en train d'éliminer les derniers ennemis faibles. Cependant, évitez de le faire si l'un des joueurs a encore des choses intéressantes et inventives qu'il voudrait essayer ou des sorts sur lesquels il se concentre : mettre un terme à une rencontre est un outil pour éviter l'ennui ; il ne faut pas l'utiliser pour réduire l'amusement d'un des joueurs. Vous pouvez considérer qu'un combat se termine plus tôt que prévu de diverses manières : les ennemis pourraient se rendre, des dégâts pourraient tuer un adversaire à qui il reste encore quelques Points de vie, ou vous pourriez tout simplement dire que le combat est terminé et que les PJ se débarrassent facilement des ennemis restants.

Un camp pourrait se rendre une fois que quasiment tous ses membres sont vaincus ou si des sorts ou des compétences les démoralisent complètement. Dès qu'une reddition a lieu, sortez de l'ordre d'initiative et entrez dans de courtes négociations. Le camp qui s'est rendu n'a guère de poids dans la discussion ; évitez donc de faire duréer les discussions trop longtemps. Ces consersations visent surtout à déterminer si les gagnants accordent quelque concession que ce soit aux perdants.

Les ennemis qui fuient peuvent constituer un problème. Les personnages joueurs veulent souvent prendre en chasse tout personne qui fuit s'ils pensent que celle-ci pourrait revenir un jour les menacer à nouveau. Évitez de jouer ces scènes mouvement par mouvement ; cela peut réellement ralentir le jeu. Si tous les adversaires fuient, sortez de l'ordre d'initiative et donnez à chaque PJ l'option de poursuivre l'un des fuyards. Le Pj peut indiquer un sort, une action ou une autre capacité à utiliser pour essayer de réduire la distance. Ensuite, comparez la Vitesse du PJ à celle de la cible, estimez à quel point le sort ou la capacité choisi par le poursuivant va l'aider et tenez compte des capacités que la proie pourrait utiliser pour s'échapper. Si vous déterminez que le poursuivant ratrappe la proie, revenez dn combat avec l'ordre d'initiative de départ. Dans le cas contraire, la proie parvient à s'échapper pour le moment.

### Gérer la durée des sorts

Les durées des sorts sont des valeurs approximatives qui réduisent les étrangetés et le hasard de la magie à des valeurs pratiques à utiliser. Cependant, cela ne signiife pas que vous pouvez utiliser votre montre pour mesurer un sort dont la durée est 1 heure. C'est l'une des raisons pour lesquelles c'est à vous qu'il revient de tenir compte librement du temps qui passe en-dehors des rencontres plutôt que d'utiliser la méthode plus rigoureuse des rounds de rencontre. S'il faut déterminer si un sort s'est terminé ou pas, c'est à vous de prendre la décision. Vous ne devriez pas être trop sévère, mais vous ne devriez pas non plus traiter les personnes comme s'ils se déplaçaient avec une précision mécanique et une efficacté parfaite entre les rencontres.

Les deux principaux moment où ces durées seront importantes sont quand les joueurs tentent de réaliser plusieurs rencontres au cours de la durée d'un sort et quand ils voudront maintenir un sort en effet pendant la rencontre.

#### Sorts sur plusieurs rencontres

Un sort durant 1 minute ne devrait couvrir plusieurs rencontre que si celles-ci se produisent les unes à côté des autres (généralement dans la même salle ou dans des salles adjacentes) et si les personnages passent directement d'un combat à l'autre. S'ils veulent s'arrêter et se soigner ou même si le groupe veut discuter d'où aller avant que le sort ne se termine, c'est suffisamment de temps perdu pour que le sort se termine.

Soyez plus généreux lorsqu'il s'agit de sorts durant 10 minutes ou plus. Un sort de 10 minutes dure suffisamment longtemps pour une rencontre, voire peut-être deux si leurs emplacements sont suffisamment proches. Un sort de 1 heure dure généralement pour plusieurs rencontres. Tout cela dépend aussi de la quantité de temps que les personnages passent à faire autre chose entre deux rencontres.

#### Avant un combat

Lancer un sort avant un combat (parfois appelé "se buffer à l'avance") donne un grand avantage aux personnages vu qu'is peuvent dédier plus de rounds de combat à des actions offensives plutôt qu'à des actions de préparation. Un tel avantage devrait vraiment faire une différence. Généralement, vous pouvez laisser chacun des joueurs lancer un sort ou se préparer d'une manière similaire s'ils ont la surprise sur leurs ennemis. Puis ils devraient lancer l'initiative.

Dans la plupart des cas, le fait de lancer des sorts peut rendre évidente la présence du groupe. Par exemple, si les personnages étaient tous cachés derrière une barricade, le bruit et les mouvement de l'incantation pourrait alerter les ennemis alors que, s'ils le faisaient dans une salle séparée derrière des portes fermées, le problème ne se poserait pas. Dans les cas où les préparations des PJ pourraient les faire découvrir, vous pourriez lancer l'initiative avant que qui que soit ne termine ses préparation. Lancer des sorts de préparation avant le combat devient un problème quand cela devient routinier et que les joueurs peuvent supposer que cela va toujours fonctionner. Ce type de plannification ne peut pas marcher dans toutes les situations !

## L'exploration

Contrairement à ce qui se passe dans les rencontres, les événements qui se déroulent pendant l'exploration vous demanderont généralement de prendre diverses décisions par vous-mêmes. L'exploration est intentionnellement moins contrôlée par des règles que les rencontres. Il est tout particulièrement important d'avoir une image mentale claire de l'environnement du groupe. Vous pouvez l'utiliser pour savoir où les joueur sse trouvent et pour décrire ce qu'ils voient, ce qu'ils entendent ainsi que toutes les autres sensations provenant du lieu où ils se sont aventurés.

Fondamentalement, l'exploration revient à récompenser les PJ lorsqu'ils se montrent curieux envers leur environnement. Encouragez-les à vraiment explorer et récompensez leur curiosité. Les choses qu'ils tentent de faire en mode exploration vous indiquent ce qui les intéresse et ce qu'ils considèrent important. En jouant, vous aurez une bonne idée des aspects de l'exploration qui intriguent certains joueurs et vous pourriez ajouter plus de ces choses dans vos aventures ou les mettre mieux en évidence dans les aventures publiées.


{: .tabletitle}
Mode exploration

<table class="table">
<tbody>
<tr>
  <th>Enjeux</th>
  <td>Faibles à modérés. Le mode exploration devrait être utilisé quand il y a un peu de danger mais aucune menace imminente. Les PJ pourraient se trouver dans un environnement où il y a de grandes chances pour qu'ils rencontrent un monstre ou un piège mais ils restent en mode exploration jusqu'à ce qu'un combat ou une autre interaction directe se déclenche.</td>
</tr>
<tr>
  <th>Échelle de temps</th>
  <td>Quan dles PJ sont en mode exploration, le temps du monde de jeu passe au même rythme que dans le monde réel autour de la table ; c'est pour cela qu'il est rarement mesuré à la seconde ou à la minute près. Vous pouvez accélérer ou ralentir le rythme utilisé si nécessaire. S'il est important de savoir exactement combien de temps s'est écoulé, vous pouvez généralement estimer le temps passé à explorer à 10 minutes près.</td>
</tr>
<tr>
  <th>Actions et réactions</th>
  <td>L'exploration ne se divise pas en rounds, mais les tactiques d'exploration supposent que les PJ passent une bonne partie de leur temps à accomplir certaines actions. S'il y a des actions spécifiques qu'ils veulent entreprendre, ils devraient vous le demander et vous deviriez décider s'il est pertinent ou non de passer en mode rencontre pour plus de détails. Les PJ peuvent réaliser toutes les réactions appropriées dont les déclencheurs se produisent.</td>
</tr>
</tbody>
</table>

### Tactiques d'exploration

En mode exploration, chaque joueur choisit une tactique d'exploration pour son personnage. Les tactiques les plus courantes sont [Fouiller](/ch9-jouer-à-pathfinder/mode-exploration.html#fouiller), [Être discret](/ch9-jouer-à-pathfinder/mode-exploration.html#être-discret), [Marche forcée](/ch9-jouer-à-pathfinder/mode-exploration.html#marche-forcée-fatigant) et [Détecter la magie](/ch9-jouer-à-pathfinder/mode-exploration.html#détecter-la-magie). Les joueurs resteront sans doute proches de ces activités standard mais il est important de leur permettre de décrire les actions de leur personnage. Les joueurs n'ont pas besoin de mémoriser les tactiques d'exploration et de les utiliser exactement telles qu'elles sont décrites. Au lieu de cela, ils peuvent décrire ce qu'ils font, puis vous déterminez quelle tactique s'applique. Cela signifie également que vous pouvez déterminer comment une tactique fonctionne si les actions du joueur sont originales.

{: .tabletitle}
Tactiques typiques

{: .table .table-sm}
| Tactiques d'exploration | | Tactiques sociales |
|:-|:-|:-|
| [Avancer à vitesse normale](/ch9-jouer-à-pathfinder/mode-exploration.html#avancer-à-vitesse-normale) | [Fouiller](/ch9-jouer-à-pathfinder/mode-exploration.html#fouiller) | [Commercer](/ch9-jouer-à-pathfinder/mode-exploration.html#commercer) |
| [Avancer sur ses gardes](/ch9-jouer-à-pathfinder/mode-exploration.html#avancer-sur-ses-gardes) | [Lancer un sort](/ch9-jouer-à-pathfinder/mode-exploration.html#lancer-un-sort-fatigant) | [Converser](/ch9-jouer-à-pathfinder/mode-exploration.html#converser) |
| [Couvrir ses traces](/ch9-jouer-à-pathfinder/mode-exploration.html#couvrir-ses-traces) | [Marche forcée](/ch9-jouer-à-pathfinder/mode-exploration.html#lancer-un-sort-fatigant) | [Socialiser](/ch9-jouer-à-pathfinder/mode-exploration.html#socialiser) |
| [Détecter la magie](/ch9-jouer-à-pathfinder/mode-exploration.html#détecter-la-magie) | [Se concentrer sur un sort](/ch9-jouer-à-pathfinder/mode-exploration.html#se-concentrer-sur-un-sort-fatigant) | [Rester vigilant](/ch9-jouer-à-pathfinder/mode-exploration.html#rester-vigilant) |
| [Être discret](/ch9-jouer-à-pathfinder/mode-exploration.html#être-discret) | [Suivre des traces](/ch9-jouer-à-pathfinder/mode-exploration.html#suivre-des-traces) | [Voler](/ch9-jouer-à-pathfinder/mode-exploration.html#voler-fatigant) |
| [Examiner](/ch9-jouer-à-pathfinder/mode-exploration.html#examiner) | | |


Pour déterminer quelle tactique s'applique, utilisez les conseils suivants. Une tactique telle que [Avancer à vitesse normale](/ch9-jouer-à-pathfinder/mode-exploration.html#avancer-à-vitesse-normale) ou [Être discret](/ch9-jouer-à-pathfinder/mode-exploration.html#être-discret), qui ne cause pas de fatigue, correspond à une même action répétée à peu près 10 fois par minute (comme [Se déplacer furtivement] pour Être discret) ou à une séquence d'actions répétées qui fonctionnent de manière similaire (comme des [Déplacements](/ch9-jouer-à-pathfinder/actions-de-base.html#se-déplacer) et des [Recherches](/ch9-jouer-à-pathfinder/actions-de-base.html#rechercher) pour Fouiller).

Une tactique fatigante, comme [Marche forcée](/ch9-jouer-à-pathfinder/mode-exploration.html#marche-forcée-fatigant), cause de la fatigue après 10 minuters. Une tactique fatigante est généralement composée d'actions réalisées à un rythme plus intense, avec à peu près 20 actions par minute (pour Marche forcée, il s'agit de 20 actions de [Déplacement](/ch9-jouer-à-pathfinder/actions-de-base.html#se-déplacer)). Toute tactique comportant des incantations de sorts cause de la fatigue après 10 minutes même si le nombre d'actions n'est pas aussi élevé. Quelqu'un qui [se Concentre sur un sort](/ch7-sorts/lancer-un-sort.html/#se-concentrer-sur-un-sort) sans se déplacer devient également fatigué.

Vous pourriez trouve qu'un joueur veut faire quelque chose qui correspond à utiliser 3 actions toutes les 6 secondes, comme cela se fait en combat. C'est possible lors des combats, mais uniquement parce que ceux-ci ne durent pas très longtemps. Une telle tactique n'est pas envisageable à plus long terme, comme pendant les périodes utilisées en mode exploration. Si quelqu'un tente de faire une telle chose en mode exploration, il vaut mieux l'interdire… ou le rendre fatigué par la tactique après 2 minutes seulement.

Parfois, le groupe pourrait mettre un terme à une tactique fatigante avant de devenir fatigué, puis reprendre la tactique fatigante. Vous pouvez remettre à zéro le compteur de 10 minute pour la fatigue si le gropue a passé suffisamment de temps à entreprendre des activités moins épuisantes. De manière générale, les personnages devraient adopter des tactiques non fatigantes pendant la même quantité de temps qu'ils ont consacrée à des tactiques fatigantes pour remettre ce compteur à zéro.

Les sections qui suivent abordent les tactiques qui nécessitent plus de décisions de votre part et vont plus loin que les introductions pour joueurs présentées dans la section [Mode exploration](/ch9-jouer-à-pathfinder/mode-exploration.html).

#### Converser

[Converser](/ch9-jouer-à-pathfinder/mode-exploration.html#converser)

#### Détecter la magie

[Détecter la magie](/ch9-jouer-à-pathfinder/mode-exploration.html#détecter-la-magie)


#### Suivre des traces

[Suivre des traces](/ch9-jouer-à-pathfinder/mode-exploration.html#suivre-des-traces)

#### Examiner 

[Examiner](/ch9-jouer-à-pathfinder/mode-exploration.html#examiner)

#### Fouiller

[Fouiller](/ch9-jouer-à-pathfinder/mode-exploration.html#fouiller)

#### Avancer à vitesse normale

[Avancer à vitesse normale](/ch9-jouer-à-pathfinder/mode-exploration.html#avancer-à-vitesse-normale)


### Déterminer un ordre de marche

### Les dangers

### Lancer l'initiative

#### Initiative après une réaction

#### Choisir le type de lancer

### Se reposer

#### Tours de garde et attaques surprises

### Préparations quotidiennes

### Terrain et climat difficiles

## Le temps libre

### Jouer le temps libre

#### Coopération

#### Tests

### Périodes de temps libre plus longues

#### Les événements

#### Progression moyenne

### Coût de la vie

### Acheter et revendre

### Repos à long terme

### Réentraînement

#### Réentraînement de dons

#### Réentraînement de compétences

#### Réentraînement de capacités de classe