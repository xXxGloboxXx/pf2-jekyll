---
title: "Inconscient"
titleEN: "Unconscious"
source: "Playtest Pathfinder"
layout: condition
summary: "aveuglé, assourdi, pris au dépourvu, à terre, -4 CA, ne peut agir, test de récupération en début de tour"
---

Vous avez été mis KO. Vous ne pouvez pas agir et vous avez les conditions [aveuglé](aveuglé.html), [assourdi](assourdi.html) et [pris au dépourvu](pris-au-dépourvu.html) ; de plus, vous subissez une pénalité de condition de -4 à la CA. Quand vous gagnez cette condition, vous tombez [à terre](à-terre.html) et lâchez les objets que vous tenez en main ou manipulez à moins que l'effet ne précise le contraire ou que le MJ ne détermine qu'au vu de la situation, ce n'est pas le cas. Vous devez effectuer un [test de récupération](/ch9-jouer-à-pathfinder/points-de-vie-et-guérison.html#jets-de-récupération) au début de chacun de vos tours.