---
title: "Mourant"
titleEN: "Dying"
source: "Playtest Pathfinder"
layout: condition
summary: "vous mourrez si la valeur est 4"
---

Vous saignez ou, du moins, vous vous trouvez à deux doigts de la mort. Mourant s'accompagne toujours d'une valeur et, si celle-ci atteint 4, vous mourez. Si vous êtes mourant, les jets de récupération provenant de la condition [inconscient](inconscient.html) déterminent si votre condition s'améliore ou pas. Si vous avez 1 Point de vie ou plus et que vous êtes conscient, la valeur de votre condition mourant décroît de 1 à la fin de chacun de vos tours.

Voir aussi [Points de vie et guérison](/ch9-jouer-à-pathfinder/points-de-vie-et-guérison.html).