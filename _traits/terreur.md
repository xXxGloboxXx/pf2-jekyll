---
title: "Terreur"
titleEN: "Fear"
source: "Playtest Pathfinder"
layout: trait
summary: "relatif à un effet de peur"
categories:
  - "effet"
---
Les effets de terreur évoquent l'émotion de peur. Les effets portant ce trait ont toujours les traits mental et émotion également.
