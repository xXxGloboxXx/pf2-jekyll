---
title: "Les armes"
source: "Playtest Pathfinder"
---

!!!! ERRATA
Page 181—In Table 6–6: Simple Ranged Weapons, in the
Sling entry, change its hands entry to “1”.