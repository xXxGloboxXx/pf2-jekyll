---
title: "Points de vie et guérison"
source: "Playtest Pathfinder"
toc: true
---

Toutes les créatures possèdent des Points de vie (pv ou PV). Votre valeur maximale de pv représente votre santé, votre motivation à accomplir des choses et votre élan héroïque lorsque vous êtes en pleine santé et bien reposé. Vos pv maximaux incluent les Points de vie gagnés au niveau 1 grâce à votre race et à votre classe, ceux que vous gagnez aux niveaux après le premier grâce à votre classe ainsi que ceux que vous obtenez grâce à d'autres sources (comme le don général [Robustesse](../donsgeneraux/robustesse.html)). Quand vous recevez des dégâts, vous réduisez vos Points de vie actuels d'un nombre égal au nombre de dégâts reçus.

Certains sorts et d'autres effets peuvent guérir les créatures vivantes ou les créatures mortes-vivantes. Quand vous êtes guéri, vous regagnez un nombre de Points de vie égal à la quantité de points de guérison obtenus, sans toutefois dépasser votre maximum de Points de vie.

La plupart des créatures, quand elles atteignent 0 Point de vie, meurent, à moins que l'attaque ne soit non létale, auquel cas elles sont mises KO pendant un bon moment (généralement 1 minute ou plus). Quand des créatures mortes-vivantes ou construites atteignent 0 Point de vie, elles sont détruites. Les personnages joueurs ne meurent pas automatiquement quand ils arrivent à 0 Points de vie. Au lieu de cela, ils sont mis KO. Les ennemis importants, les monstres puissants, les adversaires disposant de soigneurs ou de régénération et n'importe quel autre PNJ choisi par le MJ peuvent également suivre la même règle que les PJ.

### Être mis KO

Quand vous êtes réduit à 0 Point de vie, vous êtes mis KO. Lorsque cela se produit, les effets suivants s'appliquent à vous.

* Vous tombez inconscient (et gagnez la condition inconscient).
* Vous déplacez immédiatement votre position d'initiative jusqu'à juste avant la créature ou l'effet qui vous a réduit à 0 pv.
* Si l'attaque était létale, vous gagnez la condition mourant 1. Si vous possédiez déjà la condition mourant, augmentez sa valeur de 1. Si l'attaque était un coup critique, vous gagnez la condition mourant 2 (ou augmentez la valeur de condition déjà existante de 2).
* Si l'attaque était non létale, vous ne gagnez pas la condition mourant et vous n'augmentez pas la valeur de la condition mourant si vous la possédiez déjà. Vous remontez à 1 Point de vie mais restez inconscient.

Si votre condition mourant atteint la valeur 4 ou plus, vous mourrez.

#### Jets de récupération

Quand vous êtes inconscient, au début de chacun de vos tours, vous effectuez un jet de Vigueur spécial ayant pour objectif de regagner conscience et qu'on appelle jet (de sauvegarde) de récupération.

Le MJ détermine le DD de votre jet de récupération lorsque vous êtes mis KO. Ce DD vaut le DD du sort ou de la capacité qui vous a mis hors combat plus la valeur actuelle de votre condition mourant.

Si les dégâts qui vous ont amené à 0 Point de vie proviennent d'une source qui ne possède pas de DD, comme un jet d'attaque, utilisez le DD de classe de l'attaquant. Même si un DD de classe inclut généralement le modificateur de la caractéristique principale pour la classe du personnage, le MJ pourrait parfois décider qu'un autre modificateur de caractéristique est plus approprié. Par exemple, le DD de classe d'un magicien utilise généralement l'Intelligence mais, s'il met quelqu'un KO avec son bâton, le DD pourrait utiliser sa Force ou sa Dextérité. Pour les monstres, les MJ utiliseront le DD de compétence correspondant à une difficulté élevée pour le niveau du monstre (voir <mark>page 336</mark>).

Utilisez toujours le DD de récupération déterminé au moment où vous avez mis KO ; si le DD de l'attaquant change par la suite, le DD de votre jet de récupération, lui, n'est pas modifié rétroactivement.

#### Effets des jets de récupération

Les effets possibles d'un jet de récupération dépendent de votre situation, selon que vous êtes à 0 Point de vie (et risquez de mourir) ou à 1 Point de vie (stabilisé avec une chance de vous réveiller).

Si vous êtes à 0 Point de vie :
* **Réussite** Vous revenez à 1 Point de vie.
* **Échec** La valeur de votre condition mourant augmente de 1.
* **Échec critique** La valeur de votre condition mourant augmente de 2.

Si vous êtes à 1 Point de vie ou plus :
* **Réussite** Vous devenez conscient et vous pouvez effectuer votre tour normalement mais vous perdez 1 action ce tour-ci (donc, dans la plupart des cas, vous ne disposez que de 2 actions). Vous conservez la condition mourant.

#### Réduire la condition mourant

À la fin de chacun de vos tours, quand vous avez au moins 1 Point de vie et que vous êtes conscient, vous réduisez la valeur de votre condition mourant de 1. Comme pour toutes les autres conditions, lorsque la valeur de votre condition mourant atteint 0, la condition disparaît.

#### Subir des dégâts en étant inconscient

Si vous subissez des dégâts alors que vous êtes déjà inconscient, appliquez les mêmes effets que si vous aviez été mis KO par ces dégâts. Si le DD du jet de récupération pour ces nouveaux dégâts est plus élevé que votre DD de récupération actuel, ce dernier est remplacé par la valeur plus élevée.

#### Récupération héroïque

Vous pouvez dépenser 1 [Point héroïque](points-héroïques.html) pour perdre la condition mourant et revenir à 1 Point de vie (si vous êtes à 0 Point de vie), que vous soyez à deux doigts de la mort ou pas.

### Aux portes de la mort

Les conditions suivantes jouent un rôle crucial dans les règles relatives à la mort.

#### Mort

Vous n'êtes plus vivant. Vous ne pouvez pas agir ni être affecté par des sorts qui ciblent des créatures (à moins qu'ils ne ciblent spécifiquement des créatures mortes) et, pour la majorité des règles, vous êtes considérés comme un objet. Quand vous gagnez la condition mort, vous descendez à 0 Point de vie s'il vous restait des Points de vie et vous ne pouvez pas être ramené à plus de 0 Point de vie tant que vous restez mort.

#### Mourant

Vous saignez ou êtes, d'une manière ou d'une autre, à deux doigts de la mort. Mourant porte toujours une valeur et, si celle-ci atteint 4, vous mourrez. Si vous êtes mourant, les jets de récupération associés à la condition inconscient déterminent si votre état s'améliore ou empire. Si vous avez 1 Point de vie ou plus et êtes conscient, votre condition mourant décroît de 1 à la fin de votre tour chaque round.

#### Inconscient

Vous avez été mis KO. Vous ne pouvez pas agir ; vous possédez les conditions aveuglé, assourdi et pris au dépourvu et vous subissez une pénalité de condition de -4 à la CA. Quand vous gagnez cette condition, vous tombez à terre et lâchez tous les objets que vous teniez ou manipuliez à moins que l'effet n'indique explicitement le contraire ou que le MJ détermine que vous vous trouvez dans une situation où cela ne se produirait pas. Vous devez effectuer un [jet de récupération](../ch9-jouer-à-pathfinder/points-de-vie-et-guérison.html#jets-de-récupération) au début de chacun de vos tours.

### Effets de mort et mort instantanée

Certains sorts et certaines capacités peuvent vous tuer instantanément ou vous rapprochez de la mort sans nécessairement réduire vos Points de vie. Ces capacités possèdent le trait mort et utilisent généralement de l'énergie négative, l'opposé de la vie.

Si vous êtes réduit à 0 Point de vie par un effet de mort, vous êtes instantanément tué sans avoir à attendre d'atteindre mourant 4. Certains effets peuvent indiquer qu'ils vous tuent sur-le-champ, ce qui signifie que vous gagnez la condition mort sans avoir à atteindre mourant 4. D'autres capacités vous infligent directement la condition mourant sans vous réduire à 0 Point de vie ni vous causer des dégâts.

### Dégâts extrêmes

Vous mourrez sur-le-champ si vous subissez des dégâts égaux ou supérieurs au double de votre maximum de Points de vie.

### Points de vie temporaires

Certains sorts ou capacités vous donnent des Points de vie temporaires. Comptabilisez-les séparément de vos autres Points de vie et, quand vous subissez des dégâts, réduisez tout d'abord vos Points de vie temporaires. La plupart des Points de vie temporaires persistent pendant une durée limitée. Vous ne pouvez pas récupérer des Points de vie temporaires grâce à la guérison mais vous pouvez en gagner plus grâce à certaines capacités. Vous ne pouvez bénéficier de Points de vie temporaires provenant que d'une seule source à la fois : si vous gagnez des Points de vie temporaires alors que vous en avez déjà certains, choisissez entre conserver le nombre de Points de vie temporaires dont vous disposez déjà avec leur durée ou les remplacer par les nouveaux Points de vie temporaires en utilisant la nouvelle durée.
